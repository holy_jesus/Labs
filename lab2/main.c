#include <stdio.h>
#include <math.h>

int main()
{
    double x;
    printf("Enter x { -1.89 <= x <= 1.89 } -> ");
    scanf("%lf", &x);
    if (-1.89 <= x && x <= 1.89)
    {
        double y = (pow(2, x) + pow(2, -x)) / 2;
        double z = sqrt(2 + y - y * y);
        printf("y(x) = %lf\nz(y) = %lf\n", y, z);
    }
    else
        printf("x value is incorrect!\n");
    return 0;
}
