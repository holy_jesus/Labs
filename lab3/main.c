#include <stdio.h>
#include <math.h>


int main()
{
    double x, h, f;
    int i;
    i = 0;
    x = 0.0;
    printf("Введите шаг -> ");
    scanf("%lf", &h);
    printf("    x      f(x)  \n");
    printf("-----------------\n");
    while (i <= 2 / h) {
        x = i * h;
        if ((0 <= x) && (x <= 1)) {
            f = cos(x) * exp(pow(-x, 2));
        }
        else {
            f = log(x + 1) - sqrt(4 - x * x);
        };
        printf("%lf %lf\n", x, f);
        i++;
    };
    printf("    x      f(x)  \n");
    printf("-----------------\n");
    x = 0.0;
    for (i = 0; i <= 2 / h; i++) {
        x = i * h;
        if ((0 <= x) && (x <= 1)) {
            f = cos(x) * exp(pow(-x, 2));
        }
        else {
            f = log(x + 1) - sqrt(4 - x * x);
        }
        printf("%lf %lf\n", x, f);
    }
    return 0;
}