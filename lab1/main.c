#include <stdio.h>
#include <math.h>


int main()
{
    float a, b, c, x, smallest, sum;
    printf("Enter a -> ");
    scanf("%f", &a);
    printf("Enter b -> ");
    scanf("%f", &b);
    printf("Enter c -> ");
    scanf("%f", &c);
    printf("Enter x -> ");
    scanf("%f", &x);
    if ((b > a) && (c > a)) {
        smallest = a;
        sum = b + c;
    }
    else if ((a > b) && (c > b)) {
        smallest = b;
        sum = a + c;
    }
    else {
        smallest = c;
        sum = a + b;
    }
    if ((smallest <= x) && (fmod(smallest, 7) == 0)) {
        printf("Самое маленькое число: %1.2f", smallest);
    }
    else {
        printf("%1.2f частное наименьшего параметра и суммы двух оставшихся\n", smallest / sum);
    }
    return 0;
}